#!/usr/bin/env bash

# Go into our own dir
cd $(dirname "${BASH_SOURCE[0]}")

# Remove env from npm
for i in $(env); do
  if [[ $i == npm_* ]]; then
    unset $(echo $i | sed 's/=.*//')
  fi
done

# Watch & compile
./build.sh
while inotifywait -re close_write . ; do
  ./build.sh
done
