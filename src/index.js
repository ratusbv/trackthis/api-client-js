// Reserve the name
let api = module.exports = {};

// Browser usage
if ( 'object' === typeof window ) {
  window.trackthisClient = window.trackthisClient || api;
}

// Load versions
api.v1 = require('./v1');
