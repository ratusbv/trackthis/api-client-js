(function (factory) {
  var dependencies = ['ajax-request'];

  // RequireJS, NodeJS/Browserify, Browser
  if (('function' === typeof define) && define.amd) {
    define(dependencies, factory);
    // } else if ('object' === typeof module) {
    //   module.exports = factory.apply(this, dependencies.map(require));
  } else {
    let exp = 'object' === typeof window ? window : this;
    if (!exp.trackthisTransport) exp.trackthisTransport = {};
    exp.trackthisTransport.https = factory.call(exp,
      /*require('../../lib/ajax')//**/require('ajax-request')
    );
    exp.trackthisTransport.http  = exp.trackthisTransport.https;
  }


})(function (ajax) {

  return function (settings) {

    // Sanity checks
    let base = settings || {};
    if ('string' === typeof base) {
      base = {url: base};
    }
    if ('object' !== typeof base) {
      throw new Error('err-options-not-typeof-object');
    }

    return function request (options, callback) {

      // Sanity checks
      let opts = options || {};
      if ('string' === typeof opts) {
        opts = {name: opts};
      }
      if ('object' !== typeof opts) {
        throw new Error('err-options-not-typeof-object');
      }

      // Promise returning
      if ('function' !== typeof callback) {
        return new Promise(function(resolve,reject) {
          request(options,function(err,res) {
            if(err) return reject(err);
            return resolve(res);
          });
        });
      }

      // Prepend the baseurl
      opts.url    = base.url + opts.name;
      opts.method = opts.method || 'GET';

      // On http(s), we prefer an auth header
      if ( opts.auth &&
        ( 'object' === typeof opts.auth ) &&
        ( 'token'  === opts.auth.method )
      ) {
        opts.headers               = Object.assign({}, opts.headers || {});
        opts.headers.authorization = 'Bearer ' + opts.auth.token;
      }

      // Handle deep GET params
      if ( (opts.method === 'GET') && ('object' === typeof opts.data) ) {
        for ( let key in opts.data ) {
          if ( 'object' !== typeof opts.data[key] ) continue;
          if(!opts.data[key]) continue;
          opts.data[key] = JSON.stringify(opts.data[key]);
        }
      }

      // Make the call
      ajax(opts, function(err, res, body) {
        if(err) return callback(err);
        let data;
        try { data=JSON.parse(body); }
        catch(e) { data = undefined; }
        callback( undefined, {
          status: res.statusCode,
          body  : body,
          data  : data,
        })
      });
    };
  };
});
