const b64url = require('base64url');

module.exports = function (scope) {
  let login = {};
  let { ed25519, typ, alg, sha256,
        toByteArray, api, cb,
        generateSecret, ttl
      }     = scope;

  // Login by username & pre-computed secret
  login.secret = async function (username, secret, callback) {
    callback = callback || cb;

    // Generate the token to use
    let refresh   = !!scope.auth,
        kp        = ed25519.keyFromPrivate(toByteArray(secret)),
        header    = b64url(JSON.stringify({typ, alg, exp: Math.round((new Date().getTime() / 1000) + ttl)})),
        payload   = b64url(JSON.stringify({usr: username})),
        hash      = [...sha256(header + '.' + payload)],
        signature = b64url(kp.sign(hash).toDER()),
        testToken = [header, payload, signature].join('.');

    // Test the token, forcing the client to use the new one
    let account;
    try {
      account = await api.account.me({token: testToken});
    } catch (e) { return callback(new Error('err-invalid-credentials')); }

    // Save the credentials because we're logging in
    scope.auth = {token: testToken, keypair: kp};

    // Announce the world we logged in
    if (!refresh) {
      api.emit('login', account);
    }

    // Return our data for possible later use
    return callback(undefined, {token: testToken, account, secret: b64url(secret)});
  };

  // Login by username-password
  login.password = async function (username, password, callback) {
    let secret = generateSecret(username, password);
    return await login.secret(username, secret, callback);
  };

  // Login by known token & secret
  login.known = async function (newToken, newSecret, callback) {
    let refresh = !!scope.auth;
    callback    = callback || cb;

    // Verify the token
    let account;
    try {
      account = await api.account.me({token: newToken});
    } catch (e) { return callback(new Error('err-invalid-credentials')); }

    // Save credentials
    scope.auth = {token: newToken, keypair: ed25519.keyFromPrivate(toByteArray(newSecret)), account};

    // Announce the world we logged in
    if (!refresh) {
      api.emit('login', account);
    }

    return callback(undefined, {token: newToken, account, secret: newSecret });
  };

  return login;
};

