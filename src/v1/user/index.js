const b64url = require('base64url');

module.exports = function (scope) {
  let {api, cb, ed25519, toByteArray} = scope,
      user                            = {};

  // A check for being authenticated
  user.authenticated = function () {
    return !!scope.auth;
  };

  // Allow the client to detect token expiry
  user.ttl = async function (testToken, callback) {
    if ('function' === typeof testToken) {
      callback  = testToken;
      testToken = undefined;
    }
    callback  = callback || cb;
    testToken = testToken || (scope.auth && scope.auth.token) || false;
    if (!testToken) {
      return callback(new Error('err-not-logged-in'));
    }
    let header = JSON.parse(b64url.decode(testToken.split('.').shift()));
    return callback(undefined, header.exp - Math.round(new Date().getTime() / 1000));
  };

  // Logout simply destroys connection details
  user.logout = async function (callback) {
    callback   = callback || cb;
    scope.auth = false;
    api.emit('logout');
    return callback();
  };

  // Login handler container
  user.login = require('./login')(scope);

  // Set the token to use
  user.setToken = function (newToken) {
    let refresh = !!scope.auth;
    scope.auth  = Object.assign(scope.auth || {}, {token: newToken});
    if (!refresh) {
      api.emit('login');
    }
  };

  // Set the token to use
  user.setSecret = function (secret) {
    let refresh = !!scope.auth;
    scope.auth  = Object.assign(scope.auth || {}, {keypair: ed25519.keyFromPrivate(toByteArray(secret))});
    if (!refresh) {
      api.emit('login');
    }
  };

  return user;
};
