module.exports = function (options = {}) {
  const {mount} = options;
  return {
    find: mount('GET', 'transaction'),
  };
};
