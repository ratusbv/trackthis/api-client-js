module.exports = function (options = {}) {
  const {cb, runCall} = options;
  return {

    /**
     * Fetch the identity of an account
     *
     * @param {string|object} account
     * @param {function|void} callback
     *
     * @returns {Promise<void>}
     */
    get: async function getIdentity( account, callback ) {
      callback = callback || cb;

      // Sanity checks
      if (!account) return callback('No valid account given');
      if ('object' === typeof account) account = account.username;
      if ('string' !== typeof account) return callback('No valid account given');

      // Make the call
      return runCall('GET', `account/${account}/identity`, callback);
    },

    /**
     * Runs a partial update on an identity
     *
     * @param {string|object} account
     * @param {object}        data
     * @param {function|void} callback
     *
     * @returns {Promise<*>}
     */
    patch: async function patchIdentity(account, data, callback) {
      callback = callback || cb;

      // Sanity checks
      if (!account) return callback('No valid account given');
      if ('object' === typeof account) account = account.username;
      if ('string' !== typeof account) return callback('No valid account given');
      if (!data) return callback('No valid data given');
      if ('object' !== typeof data) return callback('No valid data given');

      // Make the call
      return runCall('PATCH', `account/${account}/identity`, data, callback);
    }
  };
};
