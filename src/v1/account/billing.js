module.exports = function (options = {}) {
  const {cb, runCall} = options;
  return {

    /**
     * Fetch the billing details of an account
     *
     * @param {string|object} account
     * @param {function|void} callback
     *
     * @returns {Promise<void>}
     */
    get: async function getBilling( account, callback ) {
      callback = callback || cb;

      // Sanity checks
      if (!account) return callback('No valid account given');
      if ('object' === typeof account) account = account.username;
      if ('string' !== typeof account) return callback('No valid account given');

      // Make the call
      return runCall('GET', `account/${account}/billing`, callback);
    },

    /**
     * Runs a partial update on billing details
     *
     * @param {string|object} account
     * @param {object}        data
     * @param {function|void} callback
     *
     * @returns {Promise<*>}
     */
    patch: async function patchBilling(account, data, callback) {
      callback = callback || cb;

      // Sanity checks
      if (!account) return callback('No valid account given');
      if ('object' === typeof account) account = account.username;
      if ('string' !== typeof account) return callback('No valid account given');
      if (!data) return callback('No valid data given');
      if ('object' !== typeof data) return callback('No valid data given');

      // Make the call
      return runCall('PATCH', `account/${account}/billing`, data, callback);
    }
  };
};
