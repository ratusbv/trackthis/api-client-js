module.exports = function (options = {}) {
  const {cb, runCall} = options;
  return {

    /**
     * Fetch the billing details of an account
     *
     * @param {string|object} account
     * @param {string|void}   [fingerprint]
     * @param {function|void} [callback]
     *
     * @returns {Promise<object|Array>}
     */
    get: async function getPubkey( account, fingerprint, callback ) {

      // Fingerprint is optional
      if ( 'function' === typeof fingerprint ) {
        callback    = fingerprint;
        fingerprint = undefined;
      }

      // Callback is optional
      callback = callback || cb;

      // Sanity checks
      if (!account) return callback('No valid account given');
      if ('object' === typeof account) account = account.username;
      if ('string' !== typeof account) return callback('No valid account given');

      // Build the url
      let url = `account/${account}/pubkey`;
      if ( fingerprint ) {
        url += `/${fingerprint}`;
      }

      // Make the call
      return runCall('GET', url, callback);
    },

    /**
     * Delete a single key
     *
     * @param {string|object} account
     * @param {string}        fingerprint
     * @param {function|void} [callback]
     *
     * @returns {Promise<boolean>}
     */
    delete: async function deletePubkey( account, fingerprint, callback ) {

      // Callback is optional
      callback = callback || cb;

      // Sanity checks
      if(!fingerprint) return callback('No valid fingerprint given');
      if('string' !== typeof fingerprint) return callback('No valid fingerprint given');
      if (!account) return callback('No valid account given');
      if ('object' === typeof account) account = account.username;
      if ('string' !== typeof account) return callback('No valid account given');

      // Make the call
      return runCall('DELETE', `account/${account}/pubkey/${fingerprint}`, {}, callback);
    },

    post: async function postPubkey( account, data, callback ) {

      // Callback is optional
      callback = callback || cb;

      // Sanity checks
      if (!account) return callback('No valid account given');
      if ( 'object' === typeof account) account = account.username;
      if ( 'string' !== typeof account) return callback('No valid account given');
      if (!data ) return callback('No valid key given');
      if ( 'object' !== typeof data ) return callback('No valid key given');
      if ( 'string' !== typeof data.algorithm ) return callback('No valid key given');
      if ( 'string' !== typeof data.data ) return callback('No valid key given');
      if (!(data.data.match(/^([0-9a-f]{2})+$/i)) ) return callback('No valid key given');
      if (!data.metadata ) return callback('No valid key given');
      if ( 'object' !== typeof data.metadata ) return callback('No valid key given');
      if ( 'string' !== typeof data.metadata.typ ) return callback('No valid key given');

      // Let the API decide the rest
      return runCall('POST', `account/${account}/pubkey`, data, callback);
    },
  };
};
