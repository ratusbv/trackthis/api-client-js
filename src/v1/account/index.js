module.exports = function (options = {}) {
  const {mount} = options;
  return {
    me      : mount('GET', 'account/me'),
    billing : require('./billing')(options),
    identity: require('./identity')(options),
    pubkey  : require('./pubkey')(options)
  };
};
