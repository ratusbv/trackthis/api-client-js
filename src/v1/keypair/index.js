module.exports = function (options = {}) {
  let {ed25519, toByteArray} = options;
  let keypair                = {};

  keypair.generateSecret       = require('generate-secret');
  keypair.fromSecret           = secret => ed25519.keyFromPrivate(toByteArray(secret));
  keypair.fromUsernamePassword = (username, password) => keypair.fromSecret(keypair.generateSecret(username, password));

  return keypair;
};
