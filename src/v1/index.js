const EventEmitter = require('events');

// Attach functions to api calls
async function runCall(method, name, data, callback) {
  if ('function' !== typeof api.transport) {
    return callback(new Error('err-no-transport-registered'));
  }
  if ('function' === typeof data) {
    callback = data;
    data     = undefined;
  }
  callback = callback || cb;
  let response, auth;
  if (componentOptions.auth) {
    auth = {
      method: 'token',
      token : (data && data.token) || componentOptions.auth.token
    };
  }
  try {
    response = (await api.transport({name, method, data, auth}));
    if ((response.status < 200) || (response.status >= 400)) {
      return callback(response.data);
    }
  } catch (e) {
    return callback(e);
  }
  return callback(undefined, response.data);
}

function mount(method, name, basedata, immutableData) {
  basedata      = Object.assign({}, basedata || {});
  immutableData = Object.assign({}, immutableData || {});
  return async function (data, callback) {
    data = Object.assign({}, basedata, data, immutableData);
    return runCall(method, name, data, callback);
  }
}

// Simplifying some code
function cb(err, res) {
  if (err) throw err;
  return res;
}

// Initialize the API
const api            = new EventEmitter(),
      EC             = require('elliptic').ec;
api.transport        = false;
let componentOptions = {
  api, cb, mount, runCall,
  auth          : false,
  typ           : 'JWT',
  alg           : 'ed25519',
  generateSecret: require('../../lib/generate-secret'),
  sha256        : require('../../lib/sha256'),
  toByteArray   : require('../../lib/to-byte-array'),
  ed25519       : new EC('ed25519'),
  ttl           : 7200 // 2-hour sessions
};

// Load all components
api.i18n        = mount('GET', 'i18n');
api.account     = require('./account')(componentOptions);
api.keypair     = require('./keypair')(componentOptions);
api.user        = require('./user')(componentOptions);
api.transaction = require('./transaction')(componentOptions);

// api.conversation = {
//   content: mount('GET', 'conversation/content'),
//   list   : mount('GET', 'conversation/list')
// };

// Output our produce
module.exports = api;
if ('object' === typeof window) {
  window.trackthisClient = window.trackthisClient || api;
}
