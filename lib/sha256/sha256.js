let hash = require('sha.js');

module.exports = function sha256(src, {asBuffer = false, asBytes = false, asString = false, asHex = false} = {}) {
  let result = hash('sha256').update(src);
  if (asBuffer) return result.digest();
  if (asBytes) return [...result.digest()];
  if (asString) return String.fromCharCode(...result.digest());
  if (asHex) return result.digest('hex');
  return result.digest();
};
