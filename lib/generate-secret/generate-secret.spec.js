import expect         from 'expect';
import generateSecret from './generate-secret';
import toHex          from 'to-hex';

test('Common usage', () => {
  expect(toHex(generateSecret('admin','admin'      ))).toBe('3fede75fcbbb9cc8629cfdb45c4f119e063364f6dd872de14b8408949502b02c');
  expect(toHex(generateSecret('root' ,'supersecret'))).toBe('15bff42572bf21ea03a71684f2e4183bdc519cc2bbb7327bf805b84165bb51a7');
});

test('Short output', () => {
  expect(toHex(generateSecret('admin','admin'      ,8))).toBe('3fede75fcbbb9cc8');
  expect(toHex(generateSecret('root' ,'supersecret',8))).toBe('15bff42572bf21ea');
});

test('Invalid input', () => {
  expect(toHex(generateSecret(null       ,'admin'    ))).toBeNull();
  expect(toHex(generateSecret('root'     ,null       ))).toBeNull();
  expect(toHex(generateSecret('root'     ,{foo:'bar'}))).toBeNull();
  expect(toHex(generateSecret({foo:'bar'},'admin'    ))).toBeNull();
});
