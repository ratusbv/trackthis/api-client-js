const crypto = require('pbkdf2'),
      sha256 = require('sha256');

module.exports = function generateSecret( username, password, length ) {
  length = length || 32;
  if(!username) return null;
  if(!password) return null;
  if('string'!==typeof username) return null;
  if('string'!==typeof password) return null;
  var iterations = 1000 + [...sha256(username)].reduce((r,c)=>(r*256+c)%63317,0);
  return [...crypto.pbkdf2Sync(password,username,iterations,length,'sha256')];
};
