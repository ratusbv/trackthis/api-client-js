/**
 * Simple AJAX library for testing purposes
 * Tries to mimic ajax-request
 *
 * DO NOT USE IN PRODUCTION
 *
 * @param obj
 * @param encoding
 * @param prefix
 * @returns {string}
 */

function serialize(obj, encoding = 'json', prefix = '') {
  switch (encoding) {
    case 'json':
      return JSON.stringify(obj);
    case 'url':
      return Object.keys(obj)
        .map(key => {
          let value = obj[key];
          if (undefined === value) return '';
          if (null === value) return '';
          key = prefix ? `${prefix}[${key}]` : key;
          if ('object' === typeof value) return serialize(value, encoding, key);
          if ('boolean' === typeof value) value = value ? '1' : '0';
          return encodeURIComponent(key) + '=' + encodeURIComponent(value);
        })
        .filter(v => v)
        .join('&');
    default:
      return '';
  }
}

module.exports = function (options, callback) {
  // callback: ( err, res, body => {} )

  // Sanity checks
  options = options || {};
  if ('string' === typeof options) options = {url: options};
  if ('object' !== typeof options) return callback('options not an object');
  if ('string' !== typeof options.url) return callback('url not valid');

  // Defaults
  options.method  = (options.method || 'GET').toUpperCase();
  options.headers = options.headers || {};

  // Inject data
  if (options.data) {
    if (['GET', 'DELETE'].indexOf(options.method) >= 0) {
      options.url += options.url.indexOf('?') >= 0 ? '&' : '?';
      options.url += serialize(options.data, 'url');
    } else {
      options.body                    = serialize(options.data);
      options.headers['content-type'] = 'application/json';
    }
  }

  // Fetch xhr object
  let xhr = (() => {
    try {
      return new(this.XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
    } catch (e) {}
  })();
  if (!xhr) {
    return callback('could not initialize xhr');
  }

  // Initialize listener
  xhr.onreadystatechange = ()=>{
    if ( xhr.readyState !== 4 ) return;
    callback( undefined, {
      statusCode: xhr.status,
      // body      : xhr.responseText,
      // data      : (()=>{try{return JSON.parse(xhr.responseText);}catch(e){}})()
    }, xhr.responseText);
  };

  // Send the request
  xhr.open(options.method,options.url,true);
  Object.keys(options.headers).forEach( header => xhr.setRequestHeader(header,options.headers[header]) );
  xhr.send(options.body);
};
