#!/usr/bin/env bash

# Resolve some directories
cd $(dirname "${BASH_SOURCE[0]}")
#APPROOT=$(node -e "process.stdout.write(''+require('app-root-path'));")
PATH="./node_modules/.bin:${PATH}";

echo -e "\nInstalling dependencies"
npm install &>/dev/null
npm install --no-save browserify uglify-es &>/dev/null
#BROWSERIFY="$(npm explore browserify -- pwd)/bin/cmd.js"
#UGLIFY="$(npm explore uglify-es -- pwd)/bin/uglifyjs"
mkdir -p dist

echo $UGLIFY

# Bundle code
echo -e "\nBundling code"
mv package.json package.json.tmp
echo " - dist/client.all.js"
browserify --plugin=esmify -e src/index              -o dist/client.all.js      || { mv package.json.tmp package.json ; exit 1 ; }
echo " - dist/client.v1.js"
browserify --plugin=esmify -e src/v1/index.js        -o dist/client.v1.js       || { mv package.json.tmp package.json ; exit 1 ; }
echo " - dist/transport.https.js"
browserify --plugin=esmify -e src/transport/https.js -o dist/transport.https.js || { mv package.json.tmp package.json ; exit 1 ; }
mv package.json.tmp package.json

# Uglify (uglify-es)
echo -e "\nMinifying"
echo " - dist/client.all.min.js"
uglifyjs --ecma 6 --compress --source-map --output dist/client.all.min.js      dist/client.all.js      || exit 1
echo " - dist/client.v1.min.js"
uglifyjs --ecma 6 --compress --source-map --output dist/client.v1.min.js       dist/client.v1.js       || exit 1
echo " - dist/transport.https.min.js"
uglifyjs --ecma 6 --compress --source-map --output dist/transport.https.min.js dist/transport.https.js || exit 1

# Include licenses
echo -e "\nPrepending licenses"
TMPFILE=$(mktemp)
echo " - dist/client.v1.js"
cat src/license.txt > $TMPFILE ; cat dist/client.v1.js           >> $TMPFILE ; cat $TMPFILE > dist/client.v1.js           || exit 1
echo " - dist/client.v1.min.js"
cat src/license.txt > $TMPFILE ; cat dist/client.v1.min.js       >> $TMPFILE ; cat $TMPFILE > dist/client.v1.min.js       || exit 1
echo " - dist/client.all.js"
cat src/license.txt > $TMPFILE ; cat dist/client.all.js          >> $TMPFILE ; cat $TMPFILE > dist/client.all.js          || exit 1
echo " - dist/client.all.min.js"
cat src/license.txt > $TMPFILE ; cat dist/client.all.min.js      >> $TMPFILE ; cat $TMPFILE > dist/client.all.min.js      || exit 1
echo " - dist/transport.https.js"
cat src/license.txt > $TMPFILE ; cat dist/transport.https.js     >> $TMPFILE ; cat $TMPFILE > dist/transport.https.js     || exit 1
echo " - dist/transport.https.min.js"
cat src/license.txt > $TMPFILE ; cat dist/transport.https.min.js >> $TMPFILE ; cat $TMPFILE > dist/transport.https.min.js || exit 1
rm ${TMPFILE}
